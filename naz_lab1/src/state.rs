use super::expression::Expression;

pub struct State {
    pub output: String,
}

impl Default for State {
    fn default() -> Self {
        State {
            output: String::new(),
        }
    }
}