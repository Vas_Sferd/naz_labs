use super::{
    numeral::Numeral,
    operator::Operator,
};

pub enum Expression {
    Numeral(Numeral),
    ComplexExpression(Box<ComplexExpression>),
}

impl ToString for Expression {
    fn to_string(&self) -> String {
        use Expression::*;
        match self {
            Numeral(s) => s.to_string(),
            ComplexExpression(c) => c.to_string(),
        }
    }
}

pub struct ComplexExpression {
    pub first: Expression,
    pub operator: Operator,
    pub second: Expression,
    pub has_brackets: bool,
}

impl ToString for ComplexExpression {
    fn to_string(&self) -> String {
        let ComplexExpression {first, operator, second, has_brackets} = self;
        
        let mut result = String::new();
        
        if *has_brackets {
            result.push('(');
        }

        result.push_str(&first.to_string());
        result.push_str(&operator.to_string());
        result.push_str(&second.to_string());

        if *has_brackets {
            result.push(')');
        }

        result
    }
}

pub struct ExpressionConstructor {
    
}