pub enum Operator {
    MulX10,
    
    Div,   
    Mul,
    
    Minus,
    Plus,
}


impl Operator {
    fn priority(&self) -> u32 {
        use Operator::*;
        match self {
            MulX10 => 3,
            Div | Mul => 2,
            Minus | Plus => 1,
        }
    }
}

impl ToString for Operator {
    fn to_string(&self) -> String {
        use Operator::*;
        match self {
            MulX10 => "* 10^",
            
            Div => "÷",  
            Mul => "*",
            
            Minus => "-",
            Plus => "+",
        }.to_owned()
    }
}