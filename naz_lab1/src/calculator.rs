use super::{egui, State};

pub(crate)
fn calculator_ui(ui: &mut egui::Ui, state: &mut State) {
    ui.vertical(|ui| {
        ui.label(&state.output);

        ui.group(|ui| {
            ui.horizontal(|ui| {
                if ui.button("10^x").clicked() {
                    
                }
                if ui.button("√").clicked() {
                    
                }
                if ui.button("x²").clicked() {
                    
                }
                if ui.button("1/x").clicked() {
                    
                }
            });
            ui.horizontal(|ui| {
                if ui.button("CE").clicked() {
                    
                }
                if ui.button("C").clicked() {
                    
                }
                if ui.button("<X").clicked() {
                    
                }
                if ui.button("÷").clicked() {
                    
                }
            });
            ui.horizontal(|ui| {
                if ui.button("7").clicked() {
                    
                }
                if ui.button("8").clicked() {
                    
                }
                if ui.button("9").clicked() {
                    
                }
                if ui.button("X").clicked() {
                    
                }
            });
            ui.horizontal(|ui| {
                if ui.button("4").clicked() {
                    
                }
                if ui.button("5").clicked() {
                    
                }
                if ui.button("6").clicked() {
                    
                }
                if ui.button("-").clicked() {
                    
                }
            });
            ui.horizontal(|ui| {
                if ui.button("1").clicked() {
                    
                }
                if ui.button("2").clicked() {
                    
                }
                if ui.button("3").clicked() {
                    
                }
                if ui.button("+").clicked() {
                    
                }
            });
            ui.horizontal(|ui| {
                if ui.button("±").clicked() {
                    
                }
                if ui.button("0").clicked() {
                    
                }
                if ui.button(".").clicked() {
                    
                }
                if ui.button("=").clicked() {
                    
                }
            });
        });
    });
}