use eframe::{egui, epi};

mod calculator;
mod expression;
mod numeral;
mod operator;
mod state;

use state::State;

#[derive(Default)]
struct App { state: State }

impl epi::App for App {
    fn name(&self) -> &str { "Application" }
    fn update(&mut self, ctx: &eframe::egui::CtxRef, frame: &eframe::epi::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            calculator::calculator_ui(ui, &mut self.state);
        });
            
        frame.set_window_size(ctx.used_size());

    }
}

impl App {
    pub fn start(mut self) -> ! {
        eframe::run_native(
            Box::new(self),
            eframe::NativeOptions::default(),
        )
    }
}

fn main() {
    App::default().start();
}
