use eframe::{egui, epi};

#[derive(Default)]
struct App();

impl epi::App for App {
    fn name(&self) -> &str { "Application" }
    fn update(&mut self, ctx: &eframe::egui::CtxRef, frame: &eframe::epi::Frame) {
        
    }
}

impl App {
    pub fn start(mut self) -> ! {
        eframe::run_native(
            Box::new(self),
            eframe::NativeOptions::default(),
        )
    }
}

fn main() {
    App::default().start();
}
